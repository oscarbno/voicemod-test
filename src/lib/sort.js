export const ASC = 1;
export const DESC = 2;

const __sortAscending = (a, b) => {
  if (a < b) return -1;
  if (b < a) return 1;
  return 0;
};

const __sortDescending = (a, b) => {
  if (a > b) return -1;
  if (b > a) return 1;
  return 0;
};

export default function(objects, field, direction) {
  return objects.sort((a, b) => {
    if (![ASC, DESC].includes(direction)) {
      throw new Error(
        `Invalid value for direction ${direction}. Valid values are ASC, DESC.`
      );
    }

    return direction === ASC
      ? __sortAscending(a[field], b[field])
      : __sortDescending(a[field], b[field]);
  });
}
