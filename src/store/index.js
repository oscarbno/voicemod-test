import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import { sort, sortAsc, sortDesc } from '../lib';
import voices from '../api/voices';

Vue.use(Vuex);

// Mutations
const MUTATE_SEARCH_STRING = 'mutateSearchString';
const MUTATE_VOICES = 'mutateVoices';
const MUTATE_FAVORITE_VOICES = 'mutateFavoriteVoices';
const MUTATE_SELECTED_VOICED = 'mutateSelectedVoice';
const MUTATE_FILTER_TAG = 'mutateFilterTag';
const MUTATE_SORT_VALUE = 'mutateSortValue';
// Actions
export const SET_SEARCH_STRING_ACTION = 'setSearchString';
export const FETCH_VOICES = 'fetchVoices';
export const TOGGLE_FAVORITE_VOICE = 'toggleFavoriteVoice';
export const SELECT_VOICE = 'selectVoice';
export const SET_FILTER_TAG = 'setFilterTag';
export const SET_SORT_VALUE = 'setSortValue';
// Getters
export const GET_SEARCH_STRING = 'searchString';
export const GET_VOICES = 'voices';
export const GET_FAVORITE_VOICES = 'favoriteVoices';
export const GET_SELECTED_VOICE = 'selectedVoice';
export const GET_TAGS = 'getTags';
export const GET_SELECTED_TAG = 'getSelectedTag';
export const GET_SORT_VALUES = 'getSortValues';
export const GET_SELECTED_SORT = 'getSelectedSort';

const FILTER_TAG_ALL = 'All';
const SORT_NAME_ASC = 'Name asc';
const SORT_NAME_DESC = 'Name desc';
const SORT_VALUES = [SORT_NAME_ASC, SORT_NAME_DESC];

export default new Vuex.Store({
  plugins: [createPersistedState()],

  state: {
    searchString: '',
    voices: [],
    favorites: [],
    selectedVoice: null,
    filterTag: FILTER_TAG_ALL,
    selectedSort: SORT_NAME_ASC
  },

  mutations: {
    [MUTATE_SEARCH_STRING]: (state, value) => (state.searchString = value),

    [MUTATE_VOICES]: (state, voices) => (state.voices = voices),

    [MUTATE_FAVORITE_VOICES]: (state, voices) =>
      (state.favorites = [...voices]),

    [MUTATE_SELECTED_VOICED]: (state, voice) => (state.selectedVoice = voice),

    [MUTATE_FILTER_TAG]: (state, tag) => (state.filterTag = tag),

    [MUTATE_SORT_VALUE]: (state, sort) => (state.selectedSort = sort)
  },

  actions: {
    [SET_SEARCH_STRING_ACTION]: ({ commit }, payload) => {
      const { searchString = '' } = payload;

      commit(MUTATE_SEARCH_STRING, searchString);
    },

    [FETCH_VOICES]: ({ commit }) => {
      commit(MUTATE_VOICES, voices);
    },

    [TOGGLE_FAVORITE_VOICE]: ({ commit, state }, payload) => {
      const favorites = [...state.favorites];
      const { voice } = payload;
      const index = favorites.findIndex(item => item.id === voice.id);

      index !== -1 ? favorites.splice(index, 1) : favorites.push(voice);
      commit(MUTATE_FAVORITE_VOICES, favorites);
    },

    [SELECT_VOICE]: ({ commit, state }, payload) => {
      const { voice = {} } = payload;
      const { selectedVoice } = state;
      if (selectedVoice && voice && selectedVoice.id === voice.id) {
        commit(MUTATE_SELECTED_VOICED, null);
        return;
      }

      commit(MUTATE_SELECTED_VOICED, voice);
    },

    [SET_FILTER_TAG]: ({ commit }, payload) => {
      const { tag } = payload;
      commit(MUTATE_FILTER_TAG, tag);
    },

    [SET_SORT_VALUE]: ({ commit }, payload) => {
      const { sort } = payload;
      commit(MUTATE_SORT_VALUE, sort);
    }
  },

  getters: {
    [GET_SEARCH_STRING]: state => state.searchString,

    [GET_VOICES]: state => {
      const items = state.voices.filter(
        item =>
          item.name.toLowerCase().includes(state.searchString.toLowerCase()) &&
          (state.filterTag === FILTER_TAG_ALL ||
            item.tags.includes(state.filterTag.toLowerCase()))
      );

      if (state.selectedSort === SORT_NAME_ASC) sort(items, 'name', sortAsc);
      if (state.selectedSort === SORT_NAME_DESC) sort(items, 'name', sortDesc);

      return items;
    },

    [GET_FAVORITE_VOICES]: state => state.favorites,

    [GET_SELECTED_VOICE]: state => state.selectedVoice,

    [GET_TAGS]: state => [
      FILTER_TAG_ALL,
      ...new Set(
        state.voices
          .map(item =>
            item.tags.map(
              tag => `${tag.charAt(0).toUpperCase()}${tag.slice(1)}`
            )
          )
          .flat()
      )
    ],

    [GET_SELECTED_TAG]: state => state.filterTag,

    [GET_SORT_VALUES]: () => SORT_VALUES,

    [GET_SELECTED_SORT]: state => state.selectedSort
  }
});
